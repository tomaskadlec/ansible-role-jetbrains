ansible-role-jetbrains
======================

This role eases installation of various JetBrains IDE. It installs latest
version (using information from JetBrains website). Intended use of this
role is to manage installations on multiple computers with ease. It may be
used for management of larger deployments, e.g. it is used in computer 
classrooms/labs and it is  originally written for use at FIT CTU.

Installation
------------

.. _`git submodule`: https://git-scm.com/docs/git-submodule
.. _`Ansible roles`: http://docs.ansible.com/ansible/playbooks_roles.html

Preferred way is to use it as a `git submodule`_. ::

    git submodule add https://gitlab.com/tomaskadlec/ansible-role-jetbrains.git roles/jetbrains

However, you can just simply download it and put in in ``roles`` folder of 
your project. See `Ansible roles`_ documentation for more information about 
using them.

Usage
-----

.. _hash_behaviour: http://docs.ansible.com/ansible/intro_configuration.html#hash-behaviour

At first, check Ansible settings. hash_behaviour_ must be set to ``merge`` 
(e.g. set it in your ``ansible.cfg``). It it is set you can easily select IDEs
you would like to install and provide it as a variable as shown in the 
following example: ::

    jetbrains:
        products:
            - iiu
            - phpstorm

Supported IDEs are listed bellow:

+----------+------------------------+
| Key      | IDE                    |
+----------+------------------------+
| clion    | CLion                  |
+----------+------------------------+
| iiu      | IntelliJ IDEA Ultimate |
+----------+------------------------+
| phpstorm | PhpStorm               |
+----------+------------------------+
| pycharm  | PyCharm                |
+----------+------------------------+
| rubymine | RubyMine               |
+----------+------------------------+
| webstorm | WebStorm               |
+----------+------------------------+

Include ``jetbrains`` role to install selected IDEs in your playbook and run it.

Note that older versions are removed upon successful installation of a new one.

.. vim: spelllang=en spell:
